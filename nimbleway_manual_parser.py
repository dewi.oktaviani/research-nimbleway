import requests
import json
from datetime import datetime 

MAPPING_URL_PARSERS = {
    "goersapp_detail": {
        "url" : "https://www.goersapp.com/events/market-east-festival-markeast-fest--markeastfest2024",
        "parser" : {
            "organizers": {
                "type": "list",
                "selectors": [".___GOERS___experience-detail-page__left"],
                "extractor": "text"
            }
        }
    }, 
    "wikipedia_detail": {
        "url" : "https://en.wikipedia.org/wiki/Isaac_Newton",
        "parser" : {
            "organizers": {
                "type": "list",
                "selectors": [".mw-headline"],
                "extractor": "text"
            }
        }
    }, 
}

url = 'https://api.webit.live/api/v1/realtime/web'
headers = {
    'Authorization': 'Basic YWNjb3VudC10aXB0aXBfOHpqbzVrLXBpcGVsaW5lLW5pbWJsZWFwaToyTTMwcWZjOEY4NlM=',
    'Content-Type': 'application/json'
}
data = {
    # Add your parameters here
    "parse": True, 
    "url": MAPPING_URL_PARSERS["wikipedia_detail"]["url"],
    "format": "json", 
    "render": True, 
    "country": "ID", 
    "locale": "id", 
    "parser": MAPPING_URL_PARSERS["wikipedia_detail"]["parser"]
}



response = requests.post(url, headers=headers, json=data)

print(response.status_code)
print(response.json())

ts_str = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
filename = f"{ts_str}_result_manual_parser.json"

with open(filename, 'w', newline='') as json_file:
    json.dump(response.json(), json_file)
