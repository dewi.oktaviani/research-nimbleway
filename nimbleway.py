import requests
import json
from datetime import datetime 

url = 'https://api.webit.live/api/v1/realtime/web'
headers = {
    'Authorization': 'Basic YWNjb3VudC10aXB0aXBfOHpqbzVrLXBpcGVsaW5lLW5pbWJsZWFwaToyTTMwcWZjOEY4NlM=',
    'Content-Type': 'application/json'
}
data = {
    # Add your parameters here
    "parse": True, 
    "url": "https://www.goersapp.com/events/market-east-festival-markeast-fest--markeastfest2024", 
    "format": "json", 
    "render": True, 
    "country": "ID", 
    "locale": "id"
}

response = requests.post(url, headers=headers, json=data)

print(response.status_code)
print(response.json())

ts_str = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
filename = f"{ts_str}_result.json"

with open(filename, 'w', newline='') as json_file:
    json.dump(response.json(), json_file)
